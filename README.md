**Steps to generate strings from crowdin**
---

### Crowdin

1. Access to the project
2. Build&download
3. Move downloaded folder into the convert-string folder

---

### For iOS, run the generate-script below in the terminal

```sh ios-AndroidStringExtractor.sh```

![alt text](https://bitbucket.org/moneyloverios/convert-string/raw/57bd02dbbc231e66d2156497b14de07580104ce8/Screen%20Shot%202017-06-19%20at%209.59.07%20AM.png "processing...")


**Hint:** To check the incorrect file format, use the script:

```plutil -lint Localizable.strings```

---

### Copy the language files from !output (in moneylover folder) into your project
