package zoo.android;

import com.google.gson.Gson;
import org.jsoup.Jsoup;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * String Extractor cho cụ Huy.
 * <p>
 * Created on Jan 06, 2015.
 *
 * @author kent
 */
public class StringExtractor {

    private String mSourcePath = null;
    private String mMode = null;
    private String mKeyValuePattern = null;
    private String mDefaultFolder = null;
    private String mKeyPath = null;
    private String mOutputPattern = null;
    private String mPluralSeparator = ":";
    private boolean mIncludeFirstIndex = true;
    private boolean mReplaceSpecifier = false;
    private boolean mRemoveHtmlTags = false;

    private String[] mFolders;

    private HashSet<String> mKeySet;

    public StringExtractor(String configPath) throws Exception {
        loadConfig(configPath);
        initKeys();
    }

    private void loadConfig(String configPath) throws Exception {
        try (FileInputStream configFile = new FileInputStream(configPath)) {
            Properties props = new Properties();
            props.load(configFile);

            mSourcePath = props.getProperty("sourcePath", "");

            mMode = props.getProperty("mode", "json");
            mKeyValuePattern = props.getProperty("keyValuePattern", "\"%key%\" = \"%value%\";")
                    .replace("%key%", "%1$s")
                    .replace("%value%", "%2$s");

            mDefaultFolder = props.getProperty("defaultFolder", "en-US");
            mKeyPath = props.getProperty("keyPath");

            String mFolderList = props.getProperty("sourceFolderList");
            mFolders = mFolderList.split(",");

            mOutputPattern = props.getProperty("outputFilePattern", "%name%").replace("%name%", "%s");
            mPluralSeparator = props.getProperty("pluralSeparator", ":");

            String includeOne = props.getProperty("includeFirstIndex", "true").toLowerCase();
            mIncludeFirstIndex = includeOne.equals("true");

            mReplaceSpecifier = props.getProperty("replaceFormatSpecifier", "false").toLowerCase().equals("true");

            mRemoveHtmlTags = props.getProperty("removeHtmlTags", "false").toLowerCase().equals("true");

        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("Unable to read configuration files.");
        }
    }

    /**
     * Retrieves the language keys to be processed.
     */
    private void initKeys() throws Exception {
        mKeySet = new HashSet<>();

        if (mKeyPath == null) return;

        File file = new File(mKeyPath);

        try (BufferedReader keyFile = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = keyFile.readLine()) != null) {
                mKeySet.add(line);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Unable to read key file.");
        }
    }

    /**
     * Reads Android's strings.xml in all folders and exports to the corresponding language files.
     */
    public void processAll() throws Exception {
        System.out.println("Ver: 15/06/2016.");
        System.out.println("Extracting...");
        LinkedHashMap<String, String> defaultMap = new LinkedHashMap<>();
        processFolder(mDefaultFolder, defaultMap);

        for (String folder : mFolders) {
            if (folder.trim().isEmpty() || folder.equals(mDefaultFolder)) continue; // skips default folder

            LinkedHashMap<String, String> newMap = new LinkedHashMap<>(defaultMap);
            processFolder(folder, newMap);
        }
    }

    /**
     * Processes a folder containing Android's strings.xml pertaining to a language, and exports to the corresponding
     * language file.up
     *
     * @param combinedFolderName The Android folder name and the name of the new folder to be created,
     *                           of format {@code android-folder-name:new-name}.
     * @param map                The initial key-value map to be used.
     */
    private void processFolder(String combinedFolderName, LinkedHashMap<String, String> map) throws Exception {
        String tokens[] = combinedFolderName.split(":");

        readFolder(tokens[0].trim(), map);

        switch (mMode.toLowerCase()) {
            case "json":
                writeJsonFile(tokens[tokens.length - 1].trim(), map);
                break;

            case "custom":
                writeCustomFile(tokens[tokens.length - 1].trim(), map);
                break;
        }

    }

    FilenameFilter xmlFilter = (dir, name) -> name.toLowerCase().endsWith(".xml");

    /**
     * Reads a folder containing Android's strings.xml pertaining to a language, and updates a given key-value map.
     *
     * @param folderName The Android folder name to access.
     * @param map        The key-value map to be updated.
     */
    private void readFolder(String folderName, LinkedHashMap<String, String> map) throws Exception {
        System.out.println("Processing folder: " + folderName);

        File sourceDirectory = new File(Paths.get(mSourcePath, folderName).toUri());

        if (!sourceDirectory.exists()) {
            System.out.println("    Cannot find file " + Paths.get(mSourcePath, folderName).toString());
        } else
            for (File f : sourceDirectory.listFiles(xmlFilter)) {
                //System.out.println("    Encountered file: " + f.getName());
                if (f.isFile()) readStringFile(f.toPath(), map);
            }

        for (String key : map.keySet()) {
            String value = map.get(key);

            if (value.contains("@string/")) {
                String fromKey = value.substring(8);

                if (!map.containsKey(fromKey)) {
                    throw new Exception(String.format("Failed to map '%s' for '%s'", fromKey, key));
                } else {
                    map.put(key, map.get(fromKey));
                }
            }
        }

    }

    private String formatValue(String value) {
        if (!mRemoveHtmlTags) return value;

        return Jsoup.parse(Jsoup.parse(value).text()).text();
    }

    private void readStringFile(Path filePath, LinkedHashMap<String, String> map) {

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();

        try (FileInputStream xmlFile = new FileInputStream(filePath.toFile())) {
            XMLStreamReader xmlReader = inputFactory.createXMLStreamReader(xmlFile, "UTF-8");

            while (xmlReader.hasNext()) {
                if (xmlReader.isStartElement()) {

                    String key = xmlReader.getAttributeValue(null, "name");

                    // only reads element with "name" attribute that is specified in the key list
                    if (key != null && (mKeySet.isEmpty() || mKeySet.contains(key))) {

                        String type = xmlReader.getLocalName();

                        if (type.equals("string")) {
                            try {
                                map.put(key, formatValue(xmlReader.getElementText()));
                            } catch (Exception e) {
                                System.out.println("    Error while parsing key: " + key);
                                System.out.println(e.getMessage());
                            }

                        }

                        if (type.equals("string-array")) {
                            for (StringItem item : getChildrenItem(xmlReader, "string-array")) {
                                if (item.index == 0 && !mIncludeFirstIndex)
                                    map.put(key, formatValue(item.value));
                                else
                                    map.put(key + mPluralSeparator + item.index, formatValue(item.value));
                            }
                        }

                        if (type.equals("plurals")) {
                            boolean containKeyOne = false;
                            String value = "";
                            for (StringItem item : getChildrenItem(xmlReader, "plurals")) {
                                if (item.quantity.equals("one") && !mIncludeFirstIndex) {
                                    map.put(key, formatValue(item.value));
                                    containKeyOne = true;
                                }
                                else {
                                    map.put(key + mPluralSeparator + item.quantity, formatValue(item.value));
                                    value = item.value;
                                }

                            }

                            // ThangPQ: xử lý lấy giá trị của other khi không có key "One"
                            if (!containKeyOne) {
                                map.put(key, formatValue(value));
                            }
                        }
                    }
                }

                xmlReader.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private ArrayList<StringItem> getChildrenItem(XMLStreamReader reader, String parentNode) throws XMLStreamException {
        ArrayList<StringItem> ret = new ArrayList<>();

        int index = 0;

        reader.nextTag();

        while (reader.hasNext()) {
            String type = reader.getLocalName();

            if (reader.isStartElement() && type.equals("item")) {
                String quantity = reader.getAttributeValue(null, "quantity");

                ret.add(new StringItem(index, reader.getElementText(), quantity));
                index++;
            }

            if (reader.isEndElement() && type.equals(parentNode)) {
                break;
            }

            reader.nextTag();
        }

        return ret;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void writeJsonFile(String outputFileName, LinkedHashMap<String, String> map) {
        String json = new Gson().toJson(map);

        String outputFullPath = String.format(mOutputPattern, outputFileName);
        File outputFolder = new File(outputFullPath).getParentFile();
        outputFolder.mkdirs();

        try {
            System.out.println("    Writing to " + outputFullPath);
            Files.write(Paths.get(mSourcePath, outputFullPath), json.getBytes(), StandardOpenOption.CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void writeCustomFile(String androidFolderName, LinkedHashMap<String, String> map) throws Exception {
        String outputFullString = String.format(mOutputPattern, androidFolderName);
        Path outputFullPath = Paths.get(mSourcePath, outputFullString);

        File outputFolder = new File(outputFullPath.toString()).getParentFile();
        outputFolder.mkdirs();

        try (BufferedWriter writer = Files.newBufferedWriter(
                Paths.get(mSourcePath, outputFullString),
                StandardOpenOption.CREATE
        )) {
            System.out.println("    Writing to " + outputFullString);

            for (String key : map.keySet()) {
                if (map.get(key) == null) throw new Exception("null value for key: " + key);

                String value = map.get(key).replace("\t", "").replace("\n", "\\n");

                if (mReplaceSpecifier) value = value.replace("$s", "$@");

                writer.write(String.format(mKeyValuePattern, key, value));
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        String propFile = "extractor.properties";

        if (args.length > 0) propFile = args[0];

        System.out.println("Reading config from " + propFile);

        try {
            StringExtractor extractor = new StringExtractor(propFile);
            extractor.processAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((args.length > 0 && args[args.length - 1].equals("pause"))) {
            System.out.println("Press enter to exit...");
            System.console().readLine();
        }
    }

    private class StringItem {
        public int index;
        public String value;
        public String quantity;

        public StringItem(int i, String v, String q) {
            index = i;
            value = v;
            quantity = q;
        }
    }
}
